import java.util.ArrayList;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Puhelinluettelo implements ActionListener {
	   static Scanner choice = new Scanner(System.in);
	   static ArrayList<Information> contactList = new ArrayList<Information>();  
	   private static JPanel panel;
	   private static JFrame frame;
	   private static JLabel label, label2, label3, label4;
	   private static JLabel dellabel, cantdellabel, deletelabel;
	   private static JLabel searchlabel, searchlabel2;
	   private static JTextField userText, userText2, userText3;
	   private static JButton button, clearbutton, instbutton, hideinstbutton;
	   private static JLabel instructions;
	   private static JLabel instlabel, instlabel1, instlabel2, instlabel3;
	   private static JLabel searchresult, invalidoption;
	   private static JTextArea allcontacts;
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	       boolean quit = false;
	       
	        panel = new JPanel();
	        frame = new JFrame();
	        frame.setSize(100, 100);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        frame.add(panel);

	        panel.setLayout(null);

	        label = new JLabel("Select action:");
	        label.setBounds(10, 20, 165, 25);
	        panel.add(label);
	        
	        label2 = new JLabel("Add contact name:");
	        label2.setBounds(10, 50, 165, 25);
	        panel.add(label2);
	        label2.setVisible(false);
	        
	        label3 = new JLabel("Add contact number:");
	        label3.setBounds(10, 80, 165, 25);
	        panel.add(label3);
	        label3.setVisible(false);
	        
	        label4 = new JLabel("Contact added successfully!");
	        label4.setBounds(10, 140, 185, 25);
	        panel.add(label4);
	        label4.setVisible(false);
	        
	        searchresult = new JLabel("");
	        searchresult.setBounds(10, 140, 305, 25);
	        panel.add(searchresult);
	        searchresult.setVisible(false);
	        
	        allcontacts = new JTextArea(5, 20);
	        allcontacts.setBounds(10, 80, 300, 110);
	        panel.add(allcontacts);
	        allcontacts.setVisible(false);
	        allcontacts.setEditable(false);
	        
	        deletelabel = new JLabel("Delete contact name:");
	        deletelabel.setBounds(10, 50, 165, 25);
	        panel.add(deletelabel);
	        deletelabel.setVisible(false);
	        
	        dellabel = new JLabel("Contact deleted successfully!");
	        dellabel.setBounds(10, 110, 185, 25);
	        panel.add(dellabel);
	        dellabel.setVisible(false);
	        
	        cantdellabel = new JLabel("Cannot delete contact! Contact not found!");
	        cantdellabel.setBounds(10, 110, 300, 25);
	        panel.add(cantdellabel);
	        cantdellabel.setVisible(false);
	        
	        userText = new JTextField(20);
	        userText.setBounds(140, 20, 165, 25);
	        panel.add(userText);
	        
	        userText2 = new JTextField(20);
	        userText2.setBounds(140, 50, 165, 25);
	        panel.add(userText2);
	        userText2.setVisible(false);
	        
	        userText3 = new JTextField(20);
	        userText3.setBounds(140, 80, 165, 25);
	        panel.add(userText3);
	        userText3.setVisible(false);
	        
	        searchlabel = new JLabel("Search contact: ");
	        searchlabel.setBounds(10, 50, 165, 25);
	        panel.add(searchlabel);
	        searchlabel.setVisible(false);
	        
	        searchlabel2 = new JLabel("Search result: ");
	        searchlabel2.setBounds(10, 110, 185, 25);
	        panel.add(searchlabel2);
	        searchlabel2.setVisible(false);
	        
	        invalidoption = new JLabel("Invalid option! Please try again!");
	        invalidoption.setBounds(10, 80, 300, 25);
	        panel.add(invalidoption);
	        invalidoption.setVisible(false);
	        
	        button = new JButton("Enter");
	        button.setBounds(10, 50, 100, 25);
	        button.addActionListener(new Puhelinluettelo());
	        panel.add(button);
	        
	        clearbutton = new JButton("Clear");
	        clearbutton.setBounds(10, 110, 100, 25);
	        clearbutton.addActionListener(new Puhelinluettelo());
	        panel.add(clearbutton);
	        clearbutton.setVisible(false);
	        
	        instbutton = new JButton("HELP");
	        instbutton.setBounds(140, 50, 100, 25);
	        instbutton.addActionListener(new Puhelinluettelo());
	        panel.add(instbutton);
	        
	        hideinstbutton = new JButton("HIDE");
	        hideinstbutton.setBounds(140, 50, 100, 25);
	        hideinstbutton.addActionListener(new Puhelinluettelo());
	        panel.add(hideinstbutton);
	        hideinstbutton.setVisible(false);
	        
	        instructions = new JLabel("Select one of the following actions: ");
	        instructions.setBounds(10, 80, 250, 25);
	        panel.add(instructions);
	        instructions.setVisible(false);
	        instlabel = new JLabel("0. Show all contacts");
	        instlabel.setBounds(10, 110, 185, 25);
	        panel.add(instlabel);
	        instlabel.setVisible(false);
	        instlabel1 = new JLabel("1. Add contact");
	        instlabel1.setBounds(10, 140, 185, 25);
	        panel.add(instlabel1);
	        instlabel1.setVisible(false);
	        instlabel2 = new JLabel("2. Delete contact");
	        instlabel2.setBounds(10, 170, 185, 25);
	        panel.add(instlabel2);
	        instlabel2.setVisible(false);
	        instlabel3 = new JLabel("3. Search contact");
	        instlabel3.setBounds(10, 200, 185, 25);
	        panel.add(instlabel3);
			instlabel3.setVisible(false);
	        
	        
	        frame.setVisible(true);

	        // ----While-loop---- //
	        while (!quit) {
	            startingText();
	            String choose = choice.nextLine();
	            switch (choose) {
	                case "0":
	                    showContacts();
	                    break;
	                case "1":
	                    addContact();
	                    break;
	                case "2":
	                    deleteContact();
	                    break;
	                case "3":
	                    searchContact();
	                    break;
	                case "4":
	                    quit = true;
	                    System.out.println("Shutting down....\nShutting down...\nShutting down..\nShutting down.");
	                    break;
	                default:
	                    System.out.println("Invalid command. Try again!");
	                    break;
	            }
	        }
	    }


	    public static void showContacts() {
	        for (Information contact : contactList) {
	            System.out.println(contact.toString());
	        }
	    }

	    public static void deleteContact() {        
	        System.out.print("Enter the name you want to delete from contacts: ");
	        String name = choice.nextLine();
	        Information info = new Information("", "");

	        for (int i = 0; i < contactList.size(); i++) {
	            if (contactList.get(i).name.equals(name)) {
	                info = contactList.get(i);
	                contactList.remove(i);
	            }
	        }
	        if (info.name != "") {
	            System.out.println("Deleted Information!");
	        } else {
	            System.out.println("Cannot find the contact from the list! Please try again!");
	        }
	    }
	    
	    public static void addContact() {        
	        System.out.print("Enter the name you want to add to contacts: ");
	        String name = choice.nextLine();

	        System.out.print("Enter the number you want to add to contact " +name + ": ");
	        String number = choice.nextLine();
	        
	        Information info = new Information(name, number);

	        contactList.add(info);
	    }

	    public static void searchContact() {
	        //Search for contact
	        System.out.print("Enter the name you want to search from contacts: ");
	        String name = choice.nextLine();
	        Information info = new Information("", "");

	        for (int i = 0; i < contactList.size(); i++) {
	            if (contactList.get(i).name.equals(name)) {
	                info = contactList.get(i);
	                System.out.println(contactList.get(i));
	            }
	        }
	        if (info.name == "") {
	            System.out.println("Cannot find the contact from the list! Please try again!");
	        }        
	    }

	    //Instruction text
	    public static void startingText() {
	        System.out.println("\nSelect one of the following actions:");
	        System.out.println("0. Show all the contacts");
	        System.out.println("1. Add new contact");
	        System.out.println("2. Delete contact");
	        System.out.println("3. Search contact by name");
	        System.out.println("4. Quit");
	    }
	
	@Override
	public void actionPerformed(ActionEvent e) {
        String user = userText.getText();
        String name = userText2.getText();
        String number = userText3.getText();
        
		if(e.getSource()== instbutton)
        {
        	instructions.setVisible(true);
        	instlabel.setVisible(true);
        	instlabel1.setVisible(true);
        	instlabel2.setVisible(true);
        	instlabel3.setVisible(true);
        	hideinstbutton.setVisible(true);
        	instbutton.setVisible(false);
        }
		if(e.getSource()== hideinstbutton) {
        	instructions.setVisible(false);
        	instlabel.setVisible(false);
        	instlabel1.setVisible(false);
        	instlabel2.setVisible(false);
        	instlabel3.setVisible(false);
        	hideinstbutton.setVisible(false);
        	instbutton.setVisible(true);
		}
		if(e.getSource()== button && !user.equals("")) {
        	instructions.setVisible(false);
        	instlabel.setVisible(false);
        	instlabel1.setVisible(false);
        	instlabel2.setVisible(false);
        	instlabel3.setVisible(false);
        	hideinstbutton.setVisible(false);
        	instbutton.setVisible(false);
        	
			if (user.equals("0")) {
				ArrayList<Information> showAllContacts = new ArrayList<Information>(); 
				allcontacts.setVisible(true);
		        button.setVisible(false);
		        clearbutton.setVisible(true);
		        allcontacts.setLineWrap(true);
		        clearbutton.setBounds(10, 50, 100, 25);
		        for (Information contact : contactList) { 
		            showAllContacts.add(contact);
		            allcontacts.setText("Contacts: \n" +showAllContacts);
		           
		            
		        }
			}
			else if (user.equals("1")) {
				
	        	label2.setVisible(true);
	        	userText2.setVisible(true);
	        	label3.setVisible(true);
	        	button.setBounds(10, 110, 100, 25);
	       		userText3.setVisible(true);	 
	       		
			}
	        else if (user.equals("2")) {
	        	deletelabel.setVisible(true);
	        	userText2.setVisible(true);
	        	button.setBounds(10, 80, 100, 25);
	        }
	        else if (user.equals("3")) {
	        	searchlabel.setVisible(true);
	        	userText2.setVisible(true);
	        	button.setBounds(10, 80, 100, 25);
	        }
	        else if (!user.equals("1") || !user.equals("2") || !user.equals("3")) {
	        	invalidoption.setVisible(true);
	        	instbutton.setVisible(true);
	        	userText.setText("");
	        }
		}
			
	        	

			
			
	        if (!name.equals("") && !number.equals("") && user.equals("1")) {
	        	label4.setVisible(true);
	        	
		        Information info = new Information(name, number);

		        contactList.add(info);
		        button.setVisible(false);
		        clearbutton.setVisible(true);
		        clearbutton.setBounds(10, 110, 100, 25);
	        }
	        else if (!name.equals("") && user.equals("2")) {
	        	
		        Information info = new Information("", "");

		        for (int i = 0; i < contactList.size(); i++) {
		            if (contactList.get(i).name.equals(name)) {
		                info = contactList.get(i);
		                contactList.remove(i);
		            }
		        }
		        if (info.name != "") {
		        	dellabel.setVisible(true);
			        button.setVisible(false);
			        clearbutton.setBounds(10, 80, 100, 25);
			        clearbutton.setVisible(true);
		        } else {
		        	cantdellabel.setVisible(true);
			        button.setVisible(false);
			        clearbutton.setVisible(true);
			        clearbutton.setBounds(10, 80, 100, 25);
		        }		    
	        }
	        else if (!name.equals("") && user.equals("3")) {
	        	
		        button.setVisible(false);
		        clearbutton.setVisible(true);
		        searchlabel2.setVisible(true);
		        clearbutton.setBounds(10, 80, 100, 25);
		        Information info = new Information("", "");

		        for (int i = 0; i < contactList.size(); i++) {
		            if (contactList.get(i).name.equals(name)) {
		                info = contactList.get(i);
		                searchresult.setVisible(true);
		                searchresult.setText("Found contact: " +contactList.get(i));
		            }
		        }
		        if (info.name == "") {
		            searchresult.setVisible(true);
		            searchresult.setText("Cannot find the contact!");
		        }        
	        }
	        
		
			if(e.getSource()== clearbutton) {
		        button.setVisible(true);
		        instbutton.setVisible(true);
		        button.setBounds(10, 50, 100, 25);
		        clearbutton.setVisible(false);
		        label2.setVisible(false);
		        label3.setVisible(false);
	        	userText2.setVisible(false);
	        	userText3.setVisible(false);
	        	dellabel.setVisible(false);
	        	label4.setVisible(false);
	        	searchlabel.setVisible(false);
	        	searchlabel2.setVisible(false);
	        	dellabel.setVisible(false);
	        	cantdellabel.setVisible(false);
	        	deletelabel.setVisible(false);
	        	searchresult.setVisible(false);
	        	allcontacts.setVisible(false);
	        	userText.setText("");
	        	userText2.setText("");
	        	userText3.setText("");
	        	searchresult.setText("");
		        
			}
		}
	
	
}

